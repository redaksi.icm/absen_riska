#######################################
Aplikasi Absen Siswa V 1.0 Code Igniter
########################################

Aplikasi absen siswa adalah aplikasi yang memudahkan guru untuk merekap absen siswa.
Selain memudahkan untuk merekap siswa, pelaporan absen menjadi lebih mudah dan efektif.


*******************
Cara Instalasi
*******************

- Buat Folder di htdocs dengan nama absen
- buka folder, dan buka git bash here
- ikuti perintah ini :
- git init
- git add -A
- git remote add origin https://gitlab.com/dimasfauzan300/absen_riska.git
- git pull origin master
- buka phmyadmin, lalu buat database dengan nama absen
- buka database absen, klik import, klik brwose, dan klik Go

**************************
Fitur
**************************

- Rekap siswa per semester
- Aktif dan nonaktif semester
- Rekap absen per hari
- Rekap absen per semester

**************************
Deskripsi Menu
**************************

- Data Master
 * Data Master Siswa -> berguna untuk mengolah data siswa
 * Data Master Kelas -> berguna untuk mengolah data kelas
 * Data Kelas Guru -> berguna untuk memasukan / plot data siswa ke kelas
 *Semester -> berguna untuk mengaktifkan dan menonaktifkan semester

- Absensi
 * Rekap Absen Siswa -> berguna untuk merekap dan menginputkan data absen siswa

- Cetak Rekap Absen -> berguna untuk print rekap absen siswa bisa per semester atau perhari

- Pengaturan Akun -> berguna untuk olah data akun guru

- Logout -> Keluar dari aplikasi


*******************
Server Requirements
*******************

XAMPP Version 3.2.2 or Higher

