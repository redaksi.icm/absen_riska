<section class="content-header">  
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>
</section>

    <!-- Main content -->
    <!-- /.content -->
<section class="content">
  <form method="post" action="<?php echo base_url();?>index.php/Absen/Simpan">
  <div class="row">
    <div class="col-xs-12">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilihan</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">
            <table>
              <tr style="width: 40px;">               
                <td class="text-center"><a class="btn btn-info" href="<?php echo base_url();?>index.php/Absen/lihat_absen"> <i class="fa fa-files-o"></i> Lihat Hasil Rekap Absen Hari Ini</a></td>
                <td>&nbsp </td>
                <td class="text-center"><button class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button></td>
              </tr>
            </table>
             
            <!-- /.box-body -->
          </div>
    </div>
  </div>
</div>
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Absensi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">No</th>                  
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Hadir</th>
                  <th class="text-center">Sakit</th>
                  <th class="text-center">Izin</th>
                  <th class="text-center">Alfa</th>
                </tr>
                </thead>
                <tbody>
                  <?php  $no=1; foreach ($siswa as $data) { ?>
                    <tr>
                      <td class="text-center"><?php echo $no;?></td>                      
                      <td class="text-center">
                        <?php echo $data['nis'];?>
                          <input type="hidden" name="nis[]" value="<?php echo $data['nis'];?>">                          
                        </td>
                      <td class="text-center"><?php echo $data['nama_lengkap'];?></td>
                      <td class="text-center"><input type="radio" name="absen[]" class="flat-red" value="H"></td>
                      <td class="text-center"><input type="radio" name="absen[]" class="flat-red" value="S"></td>
                      <td class="text-center"><input type="radio" name="absen[]" class="flat-red" value="I"></td>
                      <td class="text-center"><input type="radio" name="absen[]" class="flat-red" value="A"></td>
                    </tr>
                  <?php $no++; }?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      </form>
</section>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/plugins/iCheck/icheck.min.js"></script>
<script>
 // $(function () {
   // $('#example1').DataTable();
  //});

  //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
</script>