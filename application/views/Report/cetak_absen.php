<section class="content-header">  
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>
</section>

    <!-- Main content -->
    <!-- /.content -->
    <?php 
    $tgl="";
        if ($tanggal2!="") {
          $tgl=$tanggal2;
        }else{
          $tgl=date("Y-m-d");
        }
    ?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilihan</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
                <div class="row">
                  <div class="col-xs-4">
                    <form method="post" action="<?php echo base_url()?>index.php/Report/getabsentgl">
                    <label>Tanggal Absen</label><input type="date" name="tgl_absen" class="form-control">
                  </div>
                  <div class="col-xs-1" style="margin-top: 25px;">                  
                    <button class="btn btn-success">Set</button>
                  </div>
                  <div class="col-xs-4" style="margin-top: 25px;"><a href="<?php echo base_url();?>index.php/Report/print/<?php echo $tgl;?>" class="btn btn-default"><i class="fa fa-print"></i> Print</a></div>
                </form>
                </div>
              </div>
            </div>                         
            <!-- /.box-body -->
          </div>
    </div>
  </div>
</div>
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Absensi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Tanggal : <?php echo $tanggal;?>
              <br>
              <table id="" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">No</th>                  
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Hadir</th>
                  <th class="text-center">Sakit</th>
                  <th class="text-center">Izin</th>
                  <th class="text-center">Alfa</th>
                </tr>
                </thead>
                <tbody>
                  <?php  $no=1; foreach ($siswa as $data) { ?>
                    <tr>
                      <td class="text-center"><?php echo $no;?></td>                      
                      <td class="text-center">
                        <?php echo $data['nis'];?>                                                   
                        </td>
                      <td class="text-center"><?php echo $data['nama_lengkap'];?></td>
                      <td class="text-center"><?php if($data["absen"]=="H"){ ?><i class="fa fa-check"></i><?php } ?></td>
                      <td class="text-center"><?php if($data["absen"]=="S"){ ?><i class="fa fa-check"></i><?php } ?></td>
                      <td class="text-center"><?php if($data["absen"]=="I"){ ?><i class="fa fa-check"></i><?php } ?></td>
                      <td class="text-center"><?php if($data["absen"]=="A"){ ?><i class="fa fa-check"></i><?php } ?></td>
                    </tr>
                  <?php $no++; }?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
</section>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/plugins/iCheck/icheck.min.js"></script>
<script>
 // $(function () {
   // $('#example1').DataTable();
  //});

  //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
</script>